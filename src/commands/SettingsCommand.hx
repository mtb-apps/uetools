package commands;

class SettingsCommand extends CLIBase {
	public function new() {}

	/**
	 * Be more verbose
	 */
	@:flag('-v')
	public var verbose:Bool = false;

	/**
	 * Run the first setup for UETools
	 */
	@:command("init")
	public function init_command(prompt:Prompt) {
		var p = "";
		prompt.prompt(MultipleChoices('Where do you want to store the settings:', ['local', 'global'])).next(function(input) {
			p = input;
			return Noise;
		});
		switch (p) {
			case 'local':
				Log.print("Creating a local .uetools file");
				var s = new Settings();
				s.save(sys.FileSystem.absolutePath("./.uetools"));

			case 'global':
				Log.print('Creating a global .uetools file at path: ${Constants.settings_path()}');
				var s = new Settings();
				s.save(Constants.settings_path());
			case _:
		}
		// Log.print("Initializing UETools", YELLOW);
	}

	/**
	 * Run the first setup for UETools
	 */
	@:command("root")
	public function root_command(rest:Rest<String>) {
		check_settings();

		if (rest != null && rest.length >= 1) {
			if (rest.length == 1) {
				Log.debug('Setting current root to ${rest[0]}');
			}
			else {
				Log.error("Too many arguments");
			}
		}
		else {
			Log.error('No Root specified');
		}
	}

	/**
	 * Print the current settings
	 */
	@:command("print")
	public function print_command(rest:Rest<String>) {
		check_settings();
		Log.print(constants.settings);
	}

	@:defaultCommand
	public function onNothing(?rest:Rest<String>) {
		Log.error("Missing subcommand");
		help_command(rest);
	}

	/**
	 * Returns the help for the settings (set) command.
	 */
	@:command("help")
	public function help_command(?rest:Rest<String>) {
		var doc = Cli.getDoc(this);
		Log.print(get_help(doc, rest));
	}
}
