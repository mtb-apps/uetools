package commands;

import tink.cli.prompt.SysPrompt;

using hxmtb.ext.ArrayExt;

class RunCommand extends CLIBase {
	public function new() {}

	/**
	 * Be more verbose
	 */
	@:flag('--verbose')
	public var verbose:Bool = false;

	/**
	 * Be more verbose
	 */
	@:flag('--debug')
	public var debug:Bool = false;

	/**
	 * Path to either a folder or a file (uplugin, uproject)
	 * By default it looks for a file in the current directory
	 */
	@:flag('--input')
	public var input:String = null;

	@:defaultCommand
	public function onNothing(?rest:Rest<String>) {
		check_settings();
		Log.print("YO", CYAN);
		manager.runEditor(debug, input, rest);
	}

	/**
	 * Returns the help for the run command.
	 */
	@:command("help")
	public function help_command(?rest:Rest<String>) {
		var doc = Cli.getDoc(this);
		Log.print(get_help(doc, rest));
	}
}
