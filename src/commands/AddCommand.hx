package commands;

import tink.cli.prompt.SysPrompt;

using hxmtb.ext.ArrayExt;

class AddCommand extends CLIBase {
	public function new() {}

	/**
	 * Be more verbose
	 */
	@:flag('--verbose')
	public var verbose:Bool = false;

	@:defaultCommand
	public function onNothing(?rest:Rest<String>) {
		check_settings();
		for (i in rest) {
			var abspath = sys.FileSystem.absolutePath(i);
			Log.debug('Checking if ${abspath} is a project, a plugin, an engine or not accepted');
		}
	}

	@:command("plugin")
	public function plugin(prompt:Prompt) {}

	/**
	 * Returns the help for the add command.
	 */
	@:command("help")
	public function help_command(?rest:Rest<String>) {
		var doc = Cli.getDoc(this);
		Log.print(get_help(doc, rest));
	}
}
