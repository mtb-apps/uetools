package commands;

import tink.cli.prompt.SysPrompt;
import UEManager;

using hxmtb.ext.ArrayExt;

/* Packages a project or plugin.*/
class PackageCommand extends CLIBase {
	public function new() {}

	/**
	 * Be more verbose
	 */
	@:flag('--verbose')
	public var verbose:Bool = false;

	/**
	 * Output path of the package
	 * By default it outputs to the dist folder of the project's root.
	 */
	@:flag('--output')
	public var output:String = null;

	/**
	 * Path to either a folder or a file (uplugin, uproject)
	 * By default it looks for a file in the current directory
	 */
	@:flag('--input')
	public var input:String = null;

	/**
	 * Configuration type. Shipping by default.
	 */
	@:flag('--config')
	public var config:String = "Shipping";

	@:defaultCommand
	public function onNothing(?rest:Rest<String>) {
		check_settings();
		manager.packageDescriptor(input, output, config, rest);
		if (rest.length > 1) {
			Log.error("Too many commands:");
			Log.error('\t${rest}');
			help_command(rest);
		}
	}

	/**
	 * Returns the help for the package command.
	 */
	@:command("help")
	public function help_command(?rest:Rest<String>) {
		var doc = Cli.getDoc(this);
		Log.print(get_help(doc, rest));
	}
}
