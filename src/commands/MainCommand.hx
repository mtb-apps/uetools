package commands;

class MainCommand extends CLIBase {
	/** Be more verbose */
	public var verbose:Bool = false;

	public function new() {}

	// private function subargs():Array<String> {
	// 	var fargs = Sys.args();
	// 	fargs.remove(fargs[0]);
	// 	return fargs;
	// }

	/**
	 * Create a new UE4 project or plugin.
	 */
	@:command("new") public var new_command:NewCommand = new NewCommand();

	/**
	 * Add project, plugin or engine to UETools
	 * You can then access them
	 */
	@:command("add") public var add_command:AddCommand = new AddCommand();

	/**
	 * Run the given project or the one in $PWD.
	 */
	@:command("run") public var run_command:RunCommand = new RunCommand();

	/**
	 *  Packages a build of the Unreal project or plugin in the current directory.
	 */
	@:command("package") public var package_command:PackageCommand = new PackageCommand();

	/**
	 *  Set|Get UETools settings.
	 */
	@:command("set") public var set_command:SettingsCommand = new SettingsCommand();

	/**
	 *  Build the project or plugin.
	 */
	@:command public var build:BuildCommand = new BuildCommand();

	/**
	 * CLI tool to manage UE4 easily.
	 */
	@:defaultCommand
	public function help_command(rest:Rest<String>) {
		Log.debug("Running Main Command");
		if (rest != null) {
			if (rest.length > 0) {
				for (r in rest) {
					Log.error('${r} is not a recognised command.');
				}
			}
		}

		help();
	}

	/**
	 * Print informations about UETools.
	 */
	@:command public function info(prompt:Prompt) {
		var is_installed_build = constants.is_installed_build ? "Installed build" : "Source build";

		Log.print('UETool v${Constants.version} by Mel Massadian', GREEN);
		Log.print("Current configuration:", YELLOW, [UNDERLINE, BOLD]);
		Log.print('\t - Engine Version: ${constants.engine_version} (${is_installed_build})', ITALIC);
		Log.print('\t - Platform: ${constants.platform}', ITALIC);
		Log.print('\t - Editor Binary: ${constants.editor_binary}', ITALIC);
		Log.print('\t - Build Script: ${constants.build_script}', ITALIC);
		Log.print('\t - Generate Script: ${constants.generate_script}', ITALIC);
		Log.print('\t - UAT Script: ${constants.UAT_script}', ITALIC);
	}

	/**
	 * Return the UETools help.
	 */
	@:command public function help(?rest:Rest<String>) {
		var doc = Cli.getDoc(this);
		Log.print(get_help(doc, rest));
	}
}
