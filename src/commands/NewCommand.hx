package commands;

import tink.cli.prompt.SysPrompt;

using hxmtb.ext.ArrayExt;

/* Create a new UE4 project or plugin.*/
class NewCommand extends CLIBase {
	public function new() {}

	/**
	 * Be more verbose
	 */
	@:flag('--verbose')
	public var verbose:Bool = false;

	/**
	 * Use editor (for new CPP Class)
	 */
	@:flag('--editor')
	public var editor:Bool = false;

	/**Print what will be done without actually doing anything*/
	@:flag('--dry')
	public var dryRun:Bool = false;

	/**Use CPP (only valid for projects)*/
	@:flag("--cpp")
	public var cpp:Bool = true;

	/**Use Blueprints (only valid for projects)*/
	@:flag("--blueprint")
	public var blueprint:Bool = false;

	/**
	 * Creates a new UE4 Project from scratch
	 */
	@:command("project")
	public function project(prompt:Prompt) {
		// if (rest.length > 0 && rest.length < 2){
		//  	Log.print('Creating project ${rest[0]}',CYAN);
		//  	return;
		//  }
		Log.print('Rest arguments: ${Sys.args()}', CYAN);
		var args:Array<String> = Sys.args();

		if (args.length == 2) {
			return prompt.prompt(Simple('\n\t${Log.wrap('Project Name', CYAN, UNDERLINE)}')).next(function(input) {
				Log.print('\tCreating project ${input}', CYAN);
				return Noise;
			});
		}
		else if (args.length == 3) {
			var project_name = args.pop();
			Log.print('\n\tCreating project ${project_name}', CYAN);
		}
		else {
			Log.error('\n\tToo many arguments');
			help_command();
		}
		Sys.exit(0);
		return null;
	}

	/**
	 * Creates a new UE4 Plugin,
	 * It will autodetect if you are already inside a project.
	 * If it is inside a project it will add it there.
	 */
	@:command("plugin")
	public function plugin(prompt:Prompt) {
		var args:Array<String> = Sys.args();

		if (args.length == 2) {
			return prompt.prompt(Simple('Plugin Name')).next(function(input) {
				Log.print('Creating plugin ${input}', CYAN);
				return Noise;
			});
		}
		else if (args.length == 3) {
			var plugin_name = args.pop();
			Log.print('Creating plugin ${plugin_name}', CYAN);
		}
		else {
			Log.error('Too many arguments');
			help_command();
		}
		Sys.exit(0);
		return null;
	}

	/**
	 * Subcommand to create projects or plugins.
	 * It auto-detects if the current working directory,
	 * or the one provided in --input (-i) contains a project already
	 * in the case of plugins it will be added to them.
	 */
	@:defaultCommand
	public function onNothing(?rest:Rest<String>) {
		Log.error("Missing subcommand");
		help_command(rest);
	}

	/**
	 * Returns the help for the new command.
	 */
	@:command("help")
	public function help_command(?rest:Rest<String>) {
		var doc = Cli.getDoc(this);
		Log.print(get_help(doc, rest));
	}
}
