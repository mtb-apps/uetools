package commands;

import tink.cli.prompt.SysPrompt;

using hxmtb.ext.ArrayExt;

class BlankCommand extends CLIBase {
	public function new() {}

	/**
	 * Be more verbose
	 */
	@:flag('--verbose')
	public var verbose:Bool = false;

	@:defaultCommand
	public function onNothing(?rest:Rest<String>) {
		Log.debug('Rest arguments: ${rest}');
		Log.error("Missing subcommand");
		help_command(rest);
	}

	/**
	 * Returns the help for the NEW command.
	 */
	@:command("help")
	public function help_command(?rest:Rest<String>) {
		var doc = Cli.getDoc(this);
		Log.print(get_help(doc, rest));
	}
}
