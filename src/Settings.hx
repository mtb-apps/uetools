import haxe.io.Path;
import sys.FileSystem.exists;
import hxmtb.Log;
import utils.Constants;
import utils.CLIBase;
import tink.cli.Rest;
import tink.Cli;

using tink.CoreApi;

// @:forward
// abstract Settings(SettingsImpl) {
//     public function new()
//         this = new SettingsImpl();
//     // @:to function toString()
//     //     // return this.toString();
//     //     return "Prout";
// }
@:jsonParse(function(json) {
	var s = new Settings();
	s.current_engine = json.current_engine;
	s.engine_roots = json.engine_roots;
	return s;
})
@:jsonStringify(function(settings:Settings) {
	return {
		current_engine: settings.current_engine,
		engine_roots: settings.engine_roots
	};
})
class Settings {
	//- Setttings
	public var current_engine:String = "";
	public var engine_roots:Array<String> = [];

	//- Other
	public var filePath:String;

	//- Internal

	/**
	 * Return either the local or the global settings file path if any
	 * The local one taking priority over the global one.
	 * @return String
	 */
	static public function path():String {
		if (exists(".uetools")) {
			return sys.FileSystem.absolutePath(".uetools");
		}
		else {
			var sp = Constants.settings_path();
			if (exists(sp)) {
				return sp;
			}
			else {
				return null;
			};
		}
	}

	public function new() {}

	public function save(path:String) {
		var p = hx.files.Path.of(path);
		if (!p.exists()) {
			var content = tink.Json.stringify(this);
			sys.io.File.saveContent(path, content);
		}
		else {
			Log.error('Setting file already there at: ${path}, remove it first if you want to start fresh using:\n\trm ${path}');
		}
	}

	/**
	 * Load settings from a JSON file
	 * @param path
	 * @return Settings or null
	 */
	public static function load(?path:String):Settings {
		var settings_file = path == null ? Settings.path() : path;
		var out:Settings;
		if (settings_file != null) {
			if (exists(settings_file)) {
				// Log.print("Found settings, Loading them.",GREEN);
				try {
					out = tink.Json.parse(sys.io.File.getContent(settings_file));
					out.filePath = settings_file;
				}
				catch (e:Any) {
					Log.error('There was an error while loading ${settings_file}:\n');
					Log.error(e);
					return null;
				}

				validate(out);
				return out;
			}
			else {
				Log.error('${path} not found.');
			}
		}
		else {
			Log.debug('No settings to load.');
			// Sys.exit(0);
		}
		return null;
	}

	static function validate(s:Settings) {
		var valid = new Map<String, Bool>();
		valid[s.current_engine] = exists(s.current_engine);
		for (x in s.engine_roots) {
			valid[x] = exists(x);
		}

		Log.print("Validating Settings:\n\t" + valid.toString(), MAGENTA);
	}

	function add(path:String) {}

	//- Type extension

	@:keep
	public function toString():String {
		return Log.wrap('Current Settings:', CYAN, [UNDERLINE, BOLD]) + "\n\t" + Log.wrap("Settings Path:", CYAN, UNDERLINE) + " " + Log.wrap(filePath)
			+ "\n\t" + Log.wrap("Current Engine:", CYAN, UNDERLINE) + " " + Log.wrap(current_engine) + "\n\t" + Log.wrap("Engine Roots:", CYAN, UNDERLINE)
			+ " " + Log.wrap(engine_roots); // + '\n' + Log.wrap('Default Settings:',[UNDERLINE,FAINT]) + '\n\t' + Log.wrap(defaults,FAINT) ;
	}
}
