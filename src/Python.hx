import tink.Cli;
import utils.CLIBase;
import commands.*;
import UEManager;

using tink.CoreApi;

class Python {
	static function main() {
		Log.warn("Running CLI");
		if (Sys.args().indexOf("-v") != -1) {
			Log.level = Debug;
		}

		var idx = Sys.args().indexOf("-c");

		if (idx != -1) {
			try {
				var c:BuildConfiguration = Sys.args()[idx + 1];
				Log.success(c);
			}
			catch (e:Any) {
				Log.error(e);
				return;
			}
		}
	}
}
// class UEGenerator {
//   public static function
// }
