import hx.files.Dir;
import hx.files.Path;
import hxmtb.core.Singleton;
import sys.FileSystem.exists;
import hxmtb.SysUtils.runCommand;
import utils.Constants;

enum abstract BuildConfiguration(String) to String {
	var Debug;
	var DebugGame;
	var Development;
	var Shipping;
	var Test;

	@:from static function fromString(v:String):BuildConfiguration {
		return
			switch v {
				case Debug | DebugGame | Development | Shipping | Test: cast v;
				case _:
					throw Log.wrap(v, RED, [UNDERLINE, BOLD]) + " " + Log.wrap('is not a valid Configuration', RED, ITALIC);
			}
	}
}

enum DescriptorType {
	Project(path:String);
	Plugin(path:String);
}

typedef Descriptor = {
	var path:String;
	var type:DescriptorType;
}

// enum abstract Descriptor(String) from String {
// 	var Project(path:String);
// 	var Plugin(path:String);
// }

/**
 * Singleton manager shared by all commands.
 */
class UEManager extends Singleton {
	// public static var fs = new FS();
	public var constants = Constants.instance();

	private function new() {
		Log.debug("Initializing UEManager.");
	}

	/*Runs the editor for the Unreal project in the specified directory (or without a project if dir is None)*/
	public function runEditor(debug = false, ?dir:String, ?args:Array<String>) {
		if (dir == null) {
			dir = Sys.getCwd();
		}

		Log.debug('Running project in ${dir}');

		var extraFlags = debug ? ['-debug'] : [];

		if (args != null) {
			extraFlags = args.concat(extraFlags);
		}

		Log.debug('Found Flags: ${extraFlags}');

		var descriptor = getDescriptor(dir);

		if (descriptor != null) {
			switch (descriptor.type) {
				case Project(path):
					Log.debug('Found: ${path}');
					var cmd = [constants.editor_binary, path, '-stdout', '-FullStdOutLogOutput'].concat(extraFlags);
					Log.success(cmd.join(" "));
				default:
					Log.error("Only projects are accepted.");
					return;
			}
		}
		else {
			Log.error('No project found in ${dir}');
		}

		// runCommand()

		// Utility.run([self.getEditorBinary(True), projectFile, '-stdout', '-FullStdOutLogOutput'] + extraFlags, raiseOnError=True)
	}

	/*Runs the Unreal Automation Tool with the supplied arguments*/
	public function runUAT(args:Array<String>) {
		var cmd = [constants.UAT_script].concat(args);
		Log.success(cmd.join(" "));

		// Utility.run([self.getRunUATScript()] + args, cwd=self.getEngineRoot(), raiseOnError=True)
	}

	/*Packages a build of the Unreal project in the specified directory, using common packaging options*/
	public function packageProject(project_file:String, output_directory:String, configuration:BuildConfiguration = Shipping, ?extraArgs:Array<String>) {
		Log.debug('Package ${project_file} to output: ${output_directory} using the ${configuration} configuration');

		//- Strip out the `-NoCompileEditor` flag if the user has specified it, since the Development version
		//- of the Editor modules for the project are needed in order to run the commandlet that cooks content
		// extraArgs = Utility.stripArgs(extraArgs, ['-nocompileeditor'])

		//- Prevent the user from specifying multiple `-platform=` or `-targetplatform=` arguments,
		//- and use the current host platform if no platform argument was explicitly specified
		// platformArgs = Utility.findArgs(extraArgs, ['-platform=', '-targetplatform='])
		// platform = Utility.getArgValue(platformArgs[0]) if len(platformArgs) > 0 else self.getPlatformIdentifier()
		// extraArgs = Utility.stripArgs(extraArgs, platformArgs) + ['-platform={}'.format(platform)]

		//- If we are building a dedicated server then ensure the server platform is set correctly
		// serverArg = Utility.findArgs(extraArgs, ['-server'])
		// serverPlatformArg = Utility.findArgs(extraArgs, ['-serverplatform='])
		// if len(serverArg) > 0 and len(serverPlatformArg) == 0:
		//	extraArgs.append('-serverplatform={}'.format(platform))

		//- If we are packaging a Shipping build, do not include debug symbols
		if (configuration == Shipping) {
			extraArgs.push('-nodebuginfo');
		}

		//- Do not create a .pak file when packaging for HTML5
		// pakArg = '-package' if platform.upper() == 'HTML5' else '-pak'
		var pakArg = "-pak";

		//- Include the `-allmaps` flag if we are building a client target and haven't specified a list of maps
		// buildingClient = (len(Utility.findArgs(extraArgs, ['-noclient'])) == 0)
		// specifiedMaps = (len(Utility.findArgs(extraArgs, ['-MapsToCook', '-MapIniSectionsToCook'])) > 0)
		// if buildingClient == True and specifiedMaps == False:
		//	extraArgs.append('-allmaps')

		//- Invoke UAT to package the build
		// distDir = os.path.join(os.path.abspath(dir), 'dist')
		runUAT([
			'BuildCookRun', '-utf8output', '-clientconfig=' + configuration, '-serverconfig=' + configuration, '-project=' + getDescriptor(project_file).path,
			'-noP4', '-cook', '-build', '-stage', '-prereqs', pakArg, '-archive', '-archivedirectory=' + output_directory,
		].concat(extraArgs));
	}

	/* Packages a build of the Unreal plugin in the specified directory, suitable for use as a prebuilt Engine module */
	public function packagePlugin(plugin_file:String, output_directory:String, configuration:BuildConfiguration = Shipping, ?extraArgs:Array<String>) {
		//- Invoke UAT to package the build

		Log.debug('Package ${plugin_file} to output: ${output_directory} using the ${configuration} configuration');
		// distDir = Path.of(dir), 'dist';
		runUAT([
			'BuildPlugin',
			'-Plugin=' + getDescriptor(plugin_file).path,
			'-Package=' + output_directory
		].concat(extraArgs));
	}

	/*Packages a build of the Unreal project or plugin in the specified directory*/
	public function packageDescriptor(?plugin_file:String, ?output_directory:String, configuration:BuildConfiguration = Shipping, ?extraArgs:Array<String>) {
		var dir = get_dir_or_file(plugin_file);
		var dist:String;
		if (output_directory != null) {
			dist = Path.of(output_directory).toString();
		}
		else {
			dist = Path.of(dir).parent.join("dist").toString();
		}

		//- Verify that an Unreal project or plugin exists in the specified directory
		var descriptor = getDescriptor(dir);

		if (descriptor != null) {
			//- Perform the packaging step
			switch (descriptor.type) {
				case Project(path):
					packageProject(path, dist, configuration, extraArgs);
				case Plugin(path):
					packagePlugin(path, dist, configuration, extraArgs);
			}
		}
		else {
			Log.error('Cannot find plugin or project to Package in ${dir}');
		}
		// if self.isProject(descriptor):
		// 	self.packageProject(dir, args[0] if len(args) > 0 else 'Shipping', args[1:])
		// else:
		// 	self.packagePlugin(dir, args)
	}

	public function getDescriptor(?working_directory:String):Descriptor {
		var dir = Path.of(get_dir_or_file(working_directory));
		// var ueroot = Dir.of(constants.engine_root);

		if (dir.isFile()) {
			if (dir.filenameExt == "uproject") {
				return {
					type: Project(dir.toString()),
					path: dir.toString()
				}
			}
			else if (dir.filenameExt == "uplugin") {
				return {
					type: Plugin(dir.toString()),
					path: dir.toString()
				}
			}
		}

		var current = Dir.of(dir);
		Log.debug('Looking for .uproject files in ${dir}');
		var project_files = current.findFiles("*.uproject");
		if (project_files.length > 0) {
			// Log.print(project_files[0], BLUE);
			return {
				type: Project(project_files[0].toString()),
				path: project_files[0].toString()
			}
		}
		Log.debug('Looking for .uplugin files in ${dir}');

		var plugin_files = current.findFiles("*.uplugin");
		if (plugin_files.length > 0) {
			// Log.print(plugin_files[0], BLUE);
			return {
				type: Plugin(plugin_files[0].toString()),
				path: plugin_files[0].toString()
			};
		}
		Log.debug('No project or plugin found in ${dir}');
		return null;
	}

	private function get_dir_or_file(?dir:String):String {
		if (dir == null) {
			var cwd = Sys.getCwd();
			Log.debug('No dir provided, setting it to CWD (${cwd})');
			return cwd;
		}
		if (Path.of(dir).isFile()) {
			return dir;
		}

		return dir;
	}

	private function getPluginDescriptor(?dir:String) {}
}
