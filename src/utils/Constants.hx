package utils;

import sys.FileSystem.exists;
import sys.io.File;
import haxe.io.Path;
import haxe.Json;
import hx.files.File;

class Constants extends Singleton {
	public static var version = '1.1';

	public var settings:Null<Settings>;

	/* On windows, use the cmdline version. */
	public var use_cmd:Bool = false;

	public var engine_root:String;

	public var editor_binary(get, null):String;

	/*Version number of the current version of UE4*/
	public var engine_version(get, null):String;

	public var is_installed_build(get, null):Bool;

	public var build_script(get, null):String;
	public var generate_script(get, null):String;
	public var UAT_script(get, null):String;

	public var platform = #if mac "Mac"; #elseif windows "Win64"; #elseif linux "Linux"; #end

	private function new(?settings:String) {
		if (settings == null) {
			this.settings = Settings.load();
		}
		else {
			this.settings = Settings.load(settings);
		}
	}

	public static var default_settings:Settings = tink.Json.parse('{
		current_engine: "/Volumes/Untitled/_EPIC/UE_4.22",
		engine_roots: ["/Volumes/Untitled/_EPIC/UE_4.22", "/Volumes/Untitled/_EPIC/UE_4.23"]
	}');

	/**
	 * Returns the list of valid build configurations supported by UnrealBuildTool
	 * @return Array<String>
	 */
	public function validBuildConfigurations():Array<String> {
		return ['Debug', 'DebugGame', 'Development', 'Shipping', 'Test'];
	}

	function defaultThirdPartyLibs():Array<String> {
		#if mac
		return [];
		#elseif windows
		#elseif linux
		return ['libc++'];
		#end
	}

	/**
	 * Returns the path to the settings file based on OS.
	 * @return String
	 */
	inline public static function settings_path():String {
		#if mac
		return Path.join([Sys.getEnv("HOME"), ".uetools"]);
		#elseif windows
		// return Path.join([Sys.getEnv("HOME"), ".uetools"]);
		#elseif linux
		// return Path.join([Sys.getEnv("HOME"), ".uetools"]);
		#end
	}

	/* Inline engine path based on OS*/
	inline function editor_path_suffix():String {
		#if mac
		return '.app/Contents/MacOS/UE4Editor';
		#elseif windows
		return '-Cmd.exe' if cmd_version == true
		else
			'.exe';
		#elseif linux
		return "";
		#end
	}

	//- Property Getters (private)

	function get_is_installed_build():Bool {
		var installed = Path.join([settings.current_engine, "Engine", "Build", "InstalledBuild.txt"]);
		return exists(installed);
	}

	function get_engine_version():String {
		var versionFile = Path.join([settings.current_engine, 'Engine', 'Build', 'Build.version']);
		if (exists(versionFile)) {
			Log.debug('Scaning: ${versionFile}');
			var vs = Json.parse(File.of(versionFile).readAsString());
			return '${Std.string(vs.MajorVersion)}.${Std.string(vs.MinorVersion)}';
		}
		else {
			return null;
		}
	}

	function get_build_script():String {
		#if windows
		return Path.join([settings.current_engine, 'Engine', 'Build', 'BatchFiles', 'Build.bat']);
		#else
		return Path.join([settings.current_engine, 'Engine', 'Build', 'BatchFiles', platform, 'Build.sh']);
		#end
	}

	function get_UAT_script():String {
		return Path.join([
			settings.current_engine,
			'Engine',
			'Build',
			'BatchFiles',
			#if windows 'RunUAT.bat' #else 'RunUAT.sh'
			#end
		]);
	}

	/**
	 * Determines the location of the UE4Editor binary
	 * @param cmdVersion
	 * @return String
	 */
	function get_editor_binary():String {
		return Path.join([
			settings.current_engine,
			'Engine',
			'Binaries',
			platform,
			'UE4Editor' + editor_path_suffix()
		]);
	}

	function get_generate_script():String {
		var genscript = "";
		#if windows
		#else
		genscript = Path.join([
			settings.current_engine,
			'Engine',
			'Build',
			'BatchFiles',
			platform,
			'GenerateProjectFiles.sh'
		]);
		#end
		#if mac
		// Under macOS, ensure `GenerateLLDBInit.sh` has been run at least one
		var lldb = Path.join([Path.directory(genscript), 'GenerateLLDBInit.sh']);
		// Log.print('On mac ${lldb} will be executed.');
		// ! runCommand('sh ${lldb}',(e:String)->Log.print(e));
		#end
		return genscript;
	}
}
