package utils;

import tink.Cli;
import tink.cli.Rest;

using tink.CoreApi;

/**
 * Base class shared by all commands
 */
class CLIBase {
	public var manager:UEManager = UEManager.instance();
	public var constants:Constants = Constants.instance();

	public var log = Log;

	private function check_settings(fail = true) {
		if (constants.settings == null) {
			Log.print(Log.wrap("No settings found.", RED) + " You need to run " + Log.wrap("ue set init", YELLOW, BOLD) + " at least once.");
			if (fail) {
				Sys.exit(0);
			}
		}
	}

	/**
	 * Automaticaly styles the documentation of Tink CLI
	 * @param rest
	 * @return String
	 */
	private function get_help(doc:String, ?rest:Rest<String>):String {
		// var doc = Cli.getDoc(this);
		var out = doc.replace("Subcommands:", Log.wrap("Subcommands:", GREEN, [UNDERLINE, BOLD]));
		out = out.replace("Flags:", Log.wrap("Flags:", GREEN, [UNDERLINE, BOLD]));
		var lines = out.split("\n");
		var styled = "";
		for (line in lines) {
			var p = line.split(":");
			if (p.length == 2) {
				styled += Log.wrap(p[0], BOLD);
				styled += ":";
				styled += Log.wrap(p[1], ITALIC);
			}
			else if (p[0] == line) {
				styled += Log.wrap(p[0], ITALIC);
			}
			else {
				styled += line;
			}
			styled += "\n";
		}
		// Log.print(lines);
		return styled;
	}
}
